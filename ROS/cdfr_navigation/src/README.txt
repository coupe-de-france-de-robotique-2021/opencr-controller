Programmes:
	main.cpp : programme principal, envoyant les objectifs aux robots
	irSensor.cpp : analyse les intensités des capteurs ir et les convertit en distance
	manoeuvre.cpp : service permettant d'avancer et de reculer d'une distance donnée par le service
	filtre.cpp : debut du programme de filtrage dynamique des données du lidar afin de masquer un objectif cible
	
Attention : les chemins des fichiers dans le main ne sont pas dynamique et doivent etre changés 
