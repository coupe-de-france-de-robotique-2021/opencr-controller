#include <ros/ros.h>
#include <string>
#include <math.h>
#include <fstream>

#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/PoseStamped.h>

#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>


using namespace std;


void laserScanCallback(const sensor_msgs::LaserScan& msg)
{
	//ROS_INFO_STREAM(msg);
	
	geometry_msgs::PoseStamped pose;
	pose.header.frame_id= "map";
	pose.header.stamp = ros::Time::now();
	pose.pose.position.x = 0;
	pose.pose.position.y = 0;

	tf2_ros::Buffer tfBuffer;
	tf2_ros::TransformListener tfListener(tfBuffer);
	
	geometry_msgs::TransformStamped transformStamped;
	try{
		transformStamped = tfBuffer.lookupTransform("base_scan", "map",
		               ros::Time(0));
	}
	catch (tf2::TransformException &ex) {
		ROS_WARN("%s",ex.what());
		ros::Duration(1.0).sleep();
	}
	//geometry_msgs::PoseStamped poseChanged = tfBuffer.transform(pose, "base_link");
	
	ROS_INFO_STREAM(transformStamped);
	
	/*float x = poseChanged.pose.position.x;
	float y = poseChanged.pose.position.y;
	
	float demiLargeur = 0.2;	//demi largeur de gobelet
	
	float ray = sqrt(x*x+y*y);
	float angle = atan2(y, x);
	
	float phi = atan2(demiLargeur, ray);*/
	
	//float angleDebMask = msg.angle_min + angle - phi;
	//float angleFinMask = msg.angle_min + angle + phi;
}


int main(int argc, char** argv)
{
	ros::init(argc, argv, "filtre");
	ros::NodeHandle n;
	
	ros::Subscriber sub = n.subscribe("/scan_filtered", 5, laserScanCallback);
	
	while(ros::ok())
	{
		ros::spinOnce();
	}
}



