#include <ros/ros.h>
#include <string>
#include "cdfr_navigation/ApprocheSrv.h"
#include "nav_msgs/Odometry.h"
#include <math.h>

//prototype
bool approche(cdfr_navigation::ApprocheSrv::Request  &req, cdfr_navigation::ApprocheSrv::Response &res);
void odomCallback(const nav_msgs::Odometry& msg);

//variable globale
float dist = 0;	//distance parcouru (odometrie)
float init = 0;	//distance initial
bool initOk = false;	//permet d'obtenir la distance initiale

ros::Publisher pub;	//publisher de cmd_vel

//main
int main(int argc, char** argv)
{
	ros::init(argc, argv, "manoeuvre");
	ros::NodeHandle n;
	
	//abonnement a odom
	ros::Subscriber sub = n.subscribe("/odom", 10, odomCallback);
	//publisher
	pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
	
	ros::ServiceServer service = n.advertiseService("approcheSrv", approche);
	ROS_INFO("Debut approche");
	ros::spin();
	
	return 0;
}

//callback du service
bool approche(cdfr_navigation::ApprocheSrv::Request  &req, cdfr_navigation::ApprocheSrv::Response &res)
{
	initOk= false;
	dist = 0;
	float consigne = abs((float)req.dist);
	int coeff = ((float)req.dist>0) ? 1 : -1;
	ROS_INFO("request: %f", (float)req.dist);
	
	ros::Rate rate(3);

	//on attend le subscriber
	while(pub.getNumSubscribers() == 0)
		ros::spin();

	//consigne de mouvement
	while(abs(dist)<consigne)
	{
		geometry_msgs::Twist twist;
		twist.linear.x = coeff * 0.2;
		twist.angular.z = 0.0;
		pub.publish(twist);
		
		rate.sleep();
		ros::spinOnce();
	}
	geometry_msgs::Twist twist;
	twist.linear.x = 0.0;
	twist.angular.z = 0.0;
	pub.publish(twist);
	
	ros::spinOnce();
	
	res.res = 1;
	ROS_INFO("response: %d", (int)res.res);
  
	return true;
}


//callback du topic odom
void odomCallback(const nav_msgs::Odometry& msg)
{
	//initialisation
	if (!initOk)
	{
		init = sqrt((msg.pose.pose.position.x*msg.pose.pose.position.x)+(msg.pose.pose.position.y*msg.pose.pose.position.y));
		initOk=true;
	}
	dist = sqrt((msg.pose.pose.position.x*msg.pose.pose.position.x)+(msg.pose.pose.position.y*msg.pose.pose.position.y))-init;
	//ROS_INFO_STREAM(dist);
}

