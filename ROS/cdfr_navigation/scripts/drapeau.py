#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32

if __name__ == '__main__':
	rospy.init_node('drapeau', anonymous=False)
	rospy.loginfo("drapeau start")
	
	rate = rospy.Rate(1) # 10hz
	rate.sleep()
	
	pub = rospy.Publisher('flagRobot', Float32, queue_size=1)
	
	for i in range(0, 2):
		estimation = Float32()
		estimation.data = 90
		
		pub.publish(estimation)
		rate.sleep()
#		rospy.loginfo(estimation)
	
	rospy.loginfo("drapeau end")
	
