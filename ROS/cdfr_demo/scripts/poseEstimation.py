#!/usr/bin/env python
import rospy
from tf.transformations import quaternion_from_euler
from std_msgs.msg import String
from geometry_msgs.msg import PoseWithCovarianceStamped

if __name__ == '__main__':
	rospy.init_node('pose_estimation', anonymous=False)
	rospy.loginfo("2D pose estimation start")
	
	rate = rospy.Rate(1) # 10hz
	rate.sleep()
	
	pub = rospy.Publisher('initialpose', PoseWithCovarianceStamped, queue_size=10)
	
	for i in range(0, 4):
		estimation = PoseWithCovarianceStamped()
		estimation.pose.pose.position.x = 0.8
		estimation.pose.pose.position.y = 2.77
		estimation.pose.pose.position.z = 0
		[x,y,z,w]=quaternion_from_euler(0.0,0.0,-1.571)
		estimation.pose.pose.orientation.x = x
		estimation.pose.pose.orientation.y = y
		estimation.pose.pose.orientation.z = z
		estimation.pose.pose.orientation.w = w
		estimation.header.frame_id= "map"
		estimation.header.stamp=rospy.Time.now()
		
		pub.publish(estimation)
		rate.sleep()
#		rospy.loginfo(estimation)
	
	rospy.loginfo("2D pose estimation end")
	
