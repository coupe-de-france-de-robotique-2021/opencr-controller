import rospy
import struct

from sensor_msgs import point_cloud2
from sensor_msgs.msg import PointCloud2, PointField
from std_msgs.msg import Header


rospy.init_node("cloud_clearing")
pub = rospy.Publisher("cloud_in", PointCloud2, queue_size=2)

x_aim = 0.6-0.1
y_aim = -0.23-0.1

points = []
lim = 20
for i in range(lim):
    for j in range(lim):
        x = (float(i) /90) + x_aim
        y = (float(j) /90) + y_aim
        z = 1
        r = int(0.0)
        g = int(255.0)
        b = int(0.0)
        a = 255
        
        rgb = struct.unpack('I', struct.pack('BBBB', b, g, r, a))[0]
        pt = [x, y, z, rgb]
        points.append(pt)

fields = [PointField('x', 0, PointField.FLOAT32, 1),
          PointField('y', 4, PointField.FLOAT32, 1),
          PointField('z', 8, PointField.FLOAT32, 1),
          # PointField('rgb', 12, PointField.UINT32, 1),
          PointField('rgba', 12, PointField.UINT32, 1),
          ]

#print(points)

header = Header()
header.frame_id = "base_scan"
pc2 = point_cloud2.create_cloud(header, fields, points)

while not rospy.is_shutdown():
    pc2.header.stamp = rospy.Time.now()
    pub.publish(pc2)
    rospy.sleep(1.0)
