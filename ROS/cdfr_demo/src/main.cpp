#include <ros/ros.h>
#include <string>
#include <fstream>

#include "cdfr_navigation/ApprocheSrv.h"
#include <std_srvs/Empty.h>

#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

#include <tf2/LinearMath/Quaternion.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int32.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Twist.h>


///////namespaces
using namespace std;


//////////////////////
//variables globales
//////////////////////
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

//publisher
ros::Publisher pubFlag;
ros::Publisher pubPose;
ros::Publisher pubRightArm;
ros::Publisher pubLeftArm;
ros::Publisher pubCmd;

//variables
bool isMapOk = false;
bool isRobotReady = false;



//////////////////
//prototypes
//////////////////
int startApproche(float objectif);					//permet d'appeler le service approche
int clearCostmap();							//permet d'appeler le service de clear costmap
void poseEstimation();							//permet d'initialiser l'origine d'amcl

int processGoal(string file, MoveBaseClient& ac);			//processus d'attribution d'objectif de navigation à partir d'un fichier
void sendGoal(MoveBaseClient& ac, float x, float y, float theta);	//envoie d'un objectif au robot
float* convertCoord(string line);					//convertit une ligne d'un fichier en coordonnée objectif

void initFlag();							//mets le drapeau en position basse
void endFlag();							//mets le drapeau en position haute

void initArm();							//replit les bras
void deployArm();							//deplit les bras

//callback
void particleCloudCallback(const geometry_msgs::PoseArray& msg);
void robotStateCallback(const std_msgs::Int32& msg);


////////////
//main
////////////
int main(int argc, char** argv)
{
	ros::init(argc, argv, "main");
	ROS_INFO("Main start");
	ros::NodeHandle n;
		
	ros::Subscriber sub = n.subscribe("/particlecloud", 5, particleCloudCallback);
	ros::Subscriber subStart = n.subscribe("/robot_state", 5, robotStateCallback);
		
	pubCmd = n.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
	pubFlag = n.advertise<std_msgs::Float32>("/flagRobot", 10);
	pubLeftArm = n.advertise<std_msgs::Float32>("/leftArm", 10);
	pubRightArm = n.advertise<std_msgs::Float32>("/rightArm", 10);
	pubPose = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose", 10);
	
	//tell the action client that we want to spin a thread by default
	MoveBaseClient ac("move_base", true);
	//wait for the action server to come up
	while(!ac.waitForServer(ros::Duration(5.0)))
	{
		ROS_INFO("Waiting for the move_base action server to come up");
	}
	
	//initialisation
	initFlag();
	initArm();
	
	while(!isMapOk)
	{
		ros::spinOnce();
	}
	//init de la pose
	poseEstimation();


	while(!isRobotReady)
	{
		ros::spinOnce();
	}
	//demarrage
	clearCostmap();	
	
	//gobelets vert
	processGoal("//home//michael//ROS//Workspaces//CDFR//src//cdfr_navigation//coords//test.txt", ac);
				
	startApproche(-0.3);

	processGoal("//home//michael//ROS//Workspaces//CDFR//src//cdfr_navigation//coords//test2.txt", ac);
				
	startApproche(-0.3);
	
	//phare
	sendGoal(ac, 0.2, 2.6, -3.1);
	deployArm();
 	
	endFlag();

	return 0;
}

void initFlag()
{	
	ROS_INFO("Drapeau init");
	
	ros::Rate loop_rate(10);
	
	for(int i = 0; i < 3; i++)
	{
		std_msgs::Float32 msg;
		msg.data = 0.0;
		pubFlag.publish(msg);
		loop_rate.sleep();
	}	
}

void endFlag()
{	
	ROS_INFO("Drapeau end");
	
	ros::Rate loop_rate(10);
	
	for(int i = 0; i < 3; i++)
	{
		std_msgs::Float32 msg;
		msg.data = 160.0;
		pubFlag.publish(msg);
		loop_rate.sleep();
	}
	
}

void initArm()
{
	ROS_INFO("Replit des bras");
	
	std_msgs::Float32 msgRight;
	msgRight.data = 160.0;
	std_msgs::Float32 msgLeft;
	msgLeft.data = 10.0;
	
	
	pubRightArm.publish(msgRight);
	pubLeftArm.publish(msgLeft);
}

void deployArm()
{	
	ROS_INFO("Deploiment des bras");
	
	std_msgs::Float32 msgRight;
	msgRight.data = 20.0;
	std_msgs::Float32 msgLeft;
	msgLeft.data = 150.0;
	
	
	pubRightArm.publish(msgRight);
	pubLeftArm.publish(msgLeft);
}

//parcourt un fichier avec des coordonnées et les envoies en objectif
int processGoal(string file, MoveBaseClient& ac)
{
	//clear
	clearCostmap();
	
	ifstream fichier(file);
	if (fichier)
	{
		string line;

		//entete
		getline(fichier, line);

		//chemin, bouée par bouée
		while(getline(fichier, line))
		{
			ROS_INFO_STREAM(line);
			
			float* coord = convertCoord(line);
			sendGoal(ac, coord[0],  coord[1],  coord[2]);
		}
		return 0;
	}
	else
	{
		ROS_INFO("File not found");
		return 1;
	}
}


//envoie un objectif au robot, a partir de coordonnées
void sendGoal(MoveBaseClient& ac, float x, float y, float theta)
{
	move_base_msgs::MoveBaseGoal goal;

	//we'll send a goal to the robot to move 1 meter forward
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();

	goal.target_pose.pose.position.x = x;
	goal.target_pose.pose.position.y = y;
	
	tf2::Quaternion myQuaternion;
	myQuaternion.setRPY( 0, 0, theta );  // Create this quaternion from roll/pitch/yaw (in radians)
	
	goal.target_pose.pose.orientation.x = myQuaternion[0];
	goal.target_pose.pose.orientation.y = myQuaternion[1];
	goal.target_pose.pose.orientation.z = myQuaternion[2];
	goal.target_pose.pose.orientation.w = myQuaternion[3];

	ROS_INFO("Sending goal");
	ac.sendGoal(goal);

	ac.waitForResult();
	
	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
  	{
    		ROS_INFO("goal achieved");
   		//startApproche(0.1);
	}
	else
		ROS_INFO("fail");
}


//transforme une ligne en coordonnées
float* convertCoord(string line)
{
	unsigned short indexFirst = line.find("\t");
	unsigned short indexSecond = line.find("\t", indexFirst+1);
	unsigned short indexThird = line.find("\t", indexSecond+1);

	float x = stof(line.substr(0, indexFirst), nullptr);
	float y = stof(line.substr(indexFirst+1, indexSecond), nullptr);
	float theta = stof(line.substr(indexSecond+1, indexThird), nullptr);
	float ordre = stoi(line.substr(indexThird+1), nullptr);
	static float tab[4];
	tab[0] = x;
	tab[1] = y;
	tab[2] = theta;
	tab[3] = ordre;
	return tab;
}


//fonction d'appel du service approche
int startApproche(float objectif)
{
	ros::NodeHandle n;
	ros::ServiceClient client = n.serviceClient<cdfr_navigation::ApprocheSrv>("approcheSrv");
	cdfr_navigation::ApprocheSrv srv;
	srv.request.dist = objectif;
	if (client.call(srv))
	{
		ROS_INFO("Response: %d", (int)srv.response.res);
		return 0;
	}
	else
	{
		ROS_ERROR("Failed to call service approcheSrv");
		return 1;
	}
}

//fonction d'appel du service clear cost map
int clearCostmap()
{
	ros::NodeHandle n;
	ros::ServiceClient client = n.serviceClient<std_srvs::Empty>("/move_base/clear_costmaps");
	std_srvs::Empty srv;
	client.call(srv);
	if (client.call(srv))
	{
		ROS_INFO("Sucess to clear costmap");
		return 0;
	}
	else
	{
		ROS_ERROR("Failed to call service clear_costmaps service");
		return 1;
	}
}

void poseEstimation()
{
	ROS_INFO("Init pose");
	
	ros::Rate loop_rate(10);
	
	for(int i = 0; i < 3; i++)
	{
		geometry_msgs::PoseWithCovarianceStamped estimation;
		estimation.pose.pose.position.x = 0.8;
		estimation.pose.pose.position.y = 2.77;
		estimation.pose.pose.position.z = 0;
		
		tf2::Quaternion myQuaternion;
		myQuaternion.setRPY( 0, 0, -1.571);  // Create this quaternion from roll/pitch/yaw (in radians)
		
		estimation.pose.pose.orientation.x = myQuaternion[0];
		estimation.pose.pose.orientation.y = myQuaternion[1];
		estimation.pose.pose.orientation.z = myQuaternion[2];
		estimation.pose.pose.orientation.w = myQuaternion[3];
		estimation.header.frame_id= "map";
		estimation.header.stamp=ros::Time::now();
		
		pubPose.publish(estimation);
		loop_rate.sleep();
	}	
}


//////////////////
//Callback
/////////////////
void particleCloudCallback(const geometry_msgs::PoseArray& msg)
{
	ROS_INFO_STREAM("Amcl pret");
	isMapOk = true;
}

void robotStateCallback(const std_msgs::Int32& msg)
{
	if (msg.data == 1)
	{
		ROS_INFO_STREAM("Robot pret");
		isRobotReady = true;
	}
}
