#include <ros/ros.h>
#include <string>
#include <fstream>
#include <math.h>
#include <std_msgs/Int32.h>
#include <turtlebot3_msgs/SensorState.h>
#include <sensor_msgs/Range.h>
#include <ros/console.h>

using namespace std;

//variables gloables
float distR=0.0;
float distL=0.0;

//moyenne
int intR = 0;
int intL = 0;

#define MOYENNE_LEFT 15
#define MOYENNE_RIGHT 15

ros::Publisher pubRight;
ros::Publisher pubLeft;

float convertIntensityToDistance(int intensity)
{
	float dist = 3467.996*pow(intensity, -0.943)/100;
	return dist;
}

void irCallbackRight(const std_msgs::Int32& msg)
{
//	ROS_INFO_STREAM(msg.data);
	if (intR < MOYENNE_RIGHT)
	{
		if (msg.data <= 300)
			distR += 1;
		else
			distR += convertIntensityToDistance(msg.data);
		intR++;
	}
	
	if (intR == MOYENNE_RIGHT)
	{
		distR/=intR;
		intR=0;
		
		sensor_msgs::Range range;
		range.header.stamp = ros::Time::now();
		range.header.frame_id= "right_ir_link";
		range.field_of_view = 0.09;
		range.min_range = 0.1;
		range.max_range = 0.3;
		range.range = distR;
		pubRight.publish(range);
		ROS_DEBUG_STREAM("Right : " << distR); 
	}
}

void irCallbackLeft(const std_msgs::Int32& msg)
{
	//	ROS_INFO_STREAM(msg.data);
	if (intL < MOYENNE_LEFT)
	{
		if (msg.data <=300)
			distL += 1;
		else
			distL += convertIntensityToDistance(msg.data);
		intL++;
	}
	
	if (intL == MOYENNE_LEFT)
	{
		distL/=intL;
		intL=0;
	
		sensor_msgs::Range range;
		range.header.stamp = ros::Time::now();
		range.header.frame_id= "left_ir_link";
		range.field_of_view = 0.09;
		range.min_range = 0.1;
		range.max_range = 0.3;
		range.range = distL;
		pubLeft.publish(range);
		ROS_DEBUG_STREAM("Left : " << distL); 
	}
}

//main
int main(int argc, char** argv)
{
	ros::init(argc, argv, "irSensor");
	ROS_INFO("ir start");
	ros::NodeHandle n;
	
	ros::Subscriber subRight = n.subscribe("/cliff_right", 5, irCallbackRight);
	ros::Subscriber subLeft = n.subscribe("/cliff_left", 5, irCallbackLeft);
	
	pubRight = n.advertise<sensor_msgs::Range>("/range_right", 10);
	pubLeft = n.advertise<sensor_msgs::Range>("/range_left", 10);
	
	while(ros::ok())
	{
		//ROS_DEBUG_STREAM("Right : " << distR << " Left : " << distL); 
		ros::spinOnce();
	}
	
	return 0;
}
