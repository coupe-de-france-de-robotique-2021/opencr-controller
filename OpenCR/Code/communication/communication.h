byte ordre[3] = {0x01, 0x02, 0x03};

//initialisation
void initCommunication(int bauds)
{
  Serial1.begin(bauds);
}

//fonction permettant de convertir un double en byte array
byte* doubleToByteArray(double nb)
{
  static byte tab[8];
  *((double *)(tab)) = nb;
  return tab;
}

//fonction permettant de convertir un byte array en double
double byteArrayToDouble(byte tab[8])
{
  return *((double *)(tab));
}

//commande pour faire avancer le robot
void ordreAvance()
{  
  float ptC_x =0 , ptC_y=0, ptF_x=0, ptF_y=0;
  byte tab[8];                                    //buffer

  //on lit les 4 doubles
  Serial1.readBytes(tab, 8);
  ptC_x = byteArrayToDouble(tab);

  Serial1.readBytes(tab, 8);
  ptC_y = byteArrayToDouble(tab);

  Serial1.readBytes(tab, 8);
  ptF_x = byteArrayToDouble(tab);

  Serial1.readBytes(tab, 8);
  ptF_y = byteArrayToDouble(tab);

 

  Serial.println(ptC_x);
  Serial.println(ptC_y);
  Serial.println(ptF_x);
  Serial.println(ptF_y);
}

//commande pour stopper le robot
void ordreStop()
{
  //on stoppe les moteurs
  
}

//commande pour envoyer la pose du robot
void ordreSentPose()
{
  double x=5.639; 
  double y=6;
  double teta=0.1;
  
  byte *tab;    //buffer

  //trame
  Serial1.write(ordre[2]);
  
  tab = doubleToByteArray(x);
  Serial1.write(tab, 8);
  tab = doubleToByteArray(y);
  Serial1.write(tab, 8);
  tab = doubleToByteArray(teta);
  Serial1.write(tab, 8);
}

void receptTrame()
{
  if(Serial1.available())
  {
    byte buf = Serial1.read();
    
    //differents ordre
    if (buf == ordre[0])
    {
      Serial.println("Avance");
      ordreAvance();
    }
    else if (buf == ordre[1])
    {
      Serial.println("Stop");
      ordreStop();
    }
    else if(buf == ordre[2])
    {
      Serial.println("Pose");
      ordreSentPose();
    }
  }
}
