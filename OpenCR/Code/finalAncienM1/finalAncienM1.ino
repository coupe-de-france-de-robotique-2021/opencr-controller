/////////////////////////////////
///////////  Librairie
///////////////////////////////// 
#include <EEPROM.h>
#include <math.h>
#include <Servo.h>                  //  Servomoteurs
#include <IMU.h>                    //  IMU
#include <DynamixelWorkbench.h>     //  Bibliothèque liée au moteurs


/////////////////////////////////
////////////  Pins on Open CR
/////////////////////////////////

//  pin utilisés pour lire le tag ArUco
#define pinNorth 2
#define pinSouth 4
//  pin utilisé pour déterminer le côté du terrain
#define pinSide 6
//  pin utilisé pour déterminer le robot utilisé(Rocket ou Falcon)
#define pinRobot 26
//  pin utilisé pour brancher le servo moteur qui hisse les pavillons
#define pinPavillons 9
//  pin utilisé pour lire les informations du bouton de reinitialisation du servomoteur des pavillons
#define pinPushPavillon 12
//  pin utilisé pour le fin de course du démarrage du robot
#define pinFinDeCourse 58
//led user
#define ledUser 22
#define ledUser2 23



//  Détermination du type de carte utilisé (ici OPENCR) automatiquement
#if defined(__OPENCM904__)
  #define DEVICE_NAME "3" //Dynamixel on Serial3(USART3)  <-OpenCM 485EXP
#elif defined(__OPENCR__)
  #define DEVICE_NAME ""
#endif   


//  Servomoteur des pavillons
Servo servoPavillon;
int pos;

//  Initilisation des moteurs
#define BAUDRATE  57600     
#define DXL_ID_1   1        // ID of RIGHT DYNAMIXEL 
#define DXL_ID_2   2        // ID of LEFT DYNAMIXEL
DynamixelWorkbench dxl1_wb, dxl2_wb;  //  création de l'instance de chaque moteur
uint8_t dxl1_id = DXL_ID_1;
uint8_t dxl2_id = DXL_ID_2;

//  Création d'un timer (HardwareTimer est inclu dans la bibliothèque DynamixelWorkbench)
HardwareTimer Timer(TIMER_CH1);

//  Initialisation de l'IMU
cIMU    IMU;




///////////////////////////////
//////Variable gloables
///////////////////////////////
//  durée du match
#define tempsmatch 10000000

int vartest=0;                //  variable permettant de dire si le tag ArUco est lu
bool Stop=true;               //  variable mise à 1 quand le robot a fini sa marche avant
bool rearStop=true;           //  variable mise à 1 quand le robot a fini sa marche arrière
double rearDistance=0;        //  distance de marche arrière (initialisée dans le setup)
int steps=0;                  //  nombre d'étapes à réaliser par le robot (de positions désirées)
int pavillonsHisses = 0;      //  variable permettant de dire si les pavillons sont hissés ou non

bool frontPosOk = true;    //determine si le position control atteint la position final desiree
bool backPosOk = true;     //determine si rear atteint la position final

//  Coordonnées du robot
double x,y,Theta;

//  Coordonnées désirées = destination
double Pose_X_d;
double Pose_Y_d;
double Thetad;

//  Coordonnées du dernier point atteint par le robot (initialisées au point de départ du robot)
double Pose_X_0=0.22;
double Pose_Y_0=0.8;
double Theta0=0;

//  Angle initial du robot (utile pour l'utilisation de l'IMU)
double ThetaInit=Theta0;

//  Coordonnées mesurées
double ThetaIMU;
double ThetaEncoder;


//  Compteurs des bouées
int compteurRouge=4;        //  nombre de déplacement nécessaires pour charger les bouées rouges
boolean redLoaded=false;    //  détermine si les bouées rouges sont déchargées sur la ligne
int unloadRed=2;            //  nombre d'étapes pour décharger les bouées rouges
int compteurVert=8;         //  nombre de déplacement nécessaires pour charger les bouées vertes
boolean greenLoaded=false;  //  détermine si les bouées rouges sont déchargées sur la ligne
int unloadGreen=2;          //  nombre d'étapes pour décharger les bouées vertes


//  Vitesses
double V_robot=0, W_robot=0;                    //  vitesse de translation(m/s)/rotation(rad/s) du robot
double real_speed_right=0, real_speed_left=0;   //  vitesses réelles des roues gauche et droite en rad/s
int32_t get_speed_right=0,get_speed_left=0;     //  vitesses mesurées par les encoders en tour/min
double reference_speed_left=0;                  //  vitesse de consigne du moteur gauche en tour/min
double reference_speed_right=0;                 //  vitesse de consigne du moteur droit en tour/min
double Speed=5;                                 //  vitesse de consigne entrée par l'utilisateur 


//  Variables de temps
double lastSampleTime=0;    //  temps du dernier échantillon                          
double Time=0;              //  temps passé depuis le début du match (ms)
double SamplePeriod=0.05;   //  durée entre 2 échantillons
double temps;               //  temps passé depuis le début du match (ms)

// Pin et valeur de la led de l'IMU
uint8_t   led_tog = 0;
uint8_t   led_pin = 13;

//  Caractéristiques du robot
double WheelBase=0.183;     //  écartement des roues
double WheelRadius=0.033;   //  rayons des roues 

int etat=4;   //  nombre d'étapes dans le déplacement du robot pour accéder en ligne droite à sa destination

double tabIMU[4]; //  tableau permettant de récupérer les différentes données données par l'IMU 

double TD;        //  distance à la destination
double Theta1;    //  angle nécessaire pour atteindre la destination
double offset;    //  variable de compensation du drift


//  Coté de départ
bool side;   // true : gauche / bleu       // false : droite / jaune
             



/////////////////////////////
/////Fonctions
/////////////////////////////

//  Fonction d'acquisition des données de l'IMU (prise de l'exemple)
void imu()
{
  static uint32_t tTime[3];       
  static uint32_t imu_time = 0;

  if ( (temps-tTime[0]) >= 500 ) 
  { 
    tTime[0] = temps;
    digitalWrite( led_pin, led_tog );
    led_tog ^= 1;
  }

  tTime[2] = temps;  
  if( IMU.update() > 0 ) 
  {
    imu_time = temps-tTime[2];
  }

  if ( (temps-tTime[1]) >= 50 )
  { 
    tTime[1] = temps;  
    /*
    Serial.print(imu_time);
    Serial.println(" ");
    Serial.print(IMU.rpy[0]);
    Serial.print(" ");
    Serial.print(IMU.rpy[1]);
    Serial.print(" ");
    Serial.println(IMU.rpy[2]);
    */

    tabIMU[0] = imu_time ;
    tabIMU[1] = (IMU.rpy[0])*2*3.1415/360;//roll
    tabIMU[2] = (IMU.rpy[1])*2*3.1415/360;//pitch
    tabIMU[3] = (IMU.rpy[2])*2*3.1415/360;//yaw  
  }
}



//Fonction d'évaluation des vitesses des moteurs et de calcul 
//des vitesses en translation et rotation du robot
void RobotSpeedEvaluation() 
{
  //  Fonction pour récupérer les vitesses des moteurs (par odométrie)
  if (digitalRead(pinRobot) == HIGH) 
  {  
    const char *log;
    bool result = false;
    result = dxl1_wb.itemRead(dxl1_id, "Present_Velocity", &get_speed_left, &log)  && dxl2_wb.itemRead(dxl2_id, "Present_Velocity", &get_speed_right, &log);
    
    if (result == false) 
    { 
      Serial.println(log);
      Serial.println("Failed to read"); 
    }
    
    else 
    {
      //  Calcul des vitesses en rad/s
      real_speed_right = get_speed_right*0.229/60*2*PI; 
      real_speed_left = get_speed_left*0.229/60*2*PI;

      //  Calcul des vitesses du robot à partir des vitesses 
      V_robot = WheelRadius*(real_speed_left+real_speed_right)/2;
      W_robot = WheelRadius*(real_speed_right*1.1637-real_speed_left*1.1637)/WheelBase;
      //Serial.print("Vitesse du robot=");
      //Serial.println(V_robot); 
    }  
  }
  
  else 
  {
    //  même principe mais avec une fonction différente pour l'autre robot qui utilise différents moteurs
    //Serial.println("Rocket");
    const char *log;
    bool result = false;
    result = dxl1_wb.itemRead(dxl1_id, "Present_Speed", &get_speed_left, &log)  && dxl2_wb.itemRead(dxl2_id, "Present_Speed", &get_speed_right, &log);
    
    if (result == false) 
    {    
      Serial.println(log);
      Serial.println("Failed to read");   
    }
    
    else 
    {
      /*
      Serial.print("Vitesses encodeuses: ");
      Serial.print(get_speed_right);
      Serial.print(" ");
      Serial.print(get_speed_left);
      */ 
      //  à partir de 1024 le premier bit, bit de signe est à 1 et la vitesse mesurée est négative
      if (get_speed_left>1023) 
      {
        get_speed_left=1024-get_speed_left;
      }
      if (get_speed_right>1023) 
      {
        get_speed_right=1024-get_speed_right;
      }  
      real_speed_right=get_speed_right*0.114*2*PI/60;
      real_speed_left=-get_speed_left*0.114*2*PI/60;
      /*
      Serial.print("Vitesses réelles: ");
      Serial.print(real_speed_right);
      Serial.print(" ");
      Serial.print(real_speed_left);
      */
      V_robot = WheelRadius*(real_speed_left+real_speed_right)/2;
      W_robot = WheelRadius*(real_speed_right*1.1637-real_speed_left*1.1637)/WheelBase;
      /*
      Serial.print("Vitesse du robot=");
      Serial.println(V_robot);
      */ 
    } 
  }
  /*
  Serial.print("w: ");
  Serial.println(W_robot);
  Serial.print("v: ");
  Serial.println(V_robot);
  */
}


//Fonction permettant de déterminer dans quel sens va tourner 
//le robot pour atteindre son objectif le plus rapidement possible
int rotationSign(double Theta0, double Thetad) 
{  
  //Serial.println("Find the right direction of rotation");
  //  si l'angle à effectuer est supérieur à un demi-tour, il faut tourner dans le sens inverse
  if (abs(Thetad-Theta0)>PI) 
  {
    //Serial.println("TURN");
    return -1;
  }
  return 1;
}


//  Fonction d'estimation de la position du robot
void Pose_Estimation() 
{
  //  Utilisation de l'IMU pour obtenir l'angle entre le robot et le repère (plus précis que l'odométrie
  ThetaIMU = tabIMU[3] + ThetaInit;   //Theta + W_robot * SamplePeriod + ThetaInit;
  /*
  Serial.print("IMU/ENCODER:");
  Serial.print(ThetaIMU);
  Serial.print(" ");
  Serial.println(ThetaEncoder);
  */
  //ThetaEncoder=fmod((Theta+W_robot*SamplePeriod),PI); (si utilisation de l'odométrie)
  Theta=ThetaIMU;//(ThetaIMU+ThetaEncoder)/2;

  Serial.print("ici :");
  Serial.println(Theta);
  /*
  //Serial.println(Theta); 
  if (ThetaEncoder<(-3.1) || ThetaEncoder>3.1) 
  {
    Theta=ThetaEncoder;
    //Serial.println("Encoder");
  }
  else 
  {
    Theta=(ThetaIMU+ThetaEncoder)/2;
    //Serial.println("IMU");
  }
  */
  //  en changeant de côté, le repère change pour que les balises gardent les mêmes coordonnées. L'angle est inversé.
  if (side) 
  {
    y=y-(V_robot*SamplePeriod*sin(Theta))*1.1637;
  }
  if (!side) 
  {
    y=y+(V_robot*SamplePeriod*sin(Theta))*1.1637;
  }
  
  x=x+(V_robot*SamplePeriod*cos(Theta))*1.1637;
  
//  Serial.print(x);
//  Serial.print(" ");
//  Serial.print(y);
//  Serial.print(" ");
//  Serial.println(Theta); 
}


//Fonction permettant de donner le signe du nombre entré en argument
int sign(double number) 
{
  return (number > 0)? 1 : -1;
}


//  Fonction de contrôle des moteurs
void LowLevelControl()
{
  if(etat!=3)
  {
    if (digitalRead(pinRobot) == HIGH)
    {
      dxl1_wb.goalVelocity(dxl1_id, (int32_t)(((reference_speed_left*60)/(2*PI))/0.229));
      dxl2_wb.goalVelocity(dxl2_id, (int32_t)(((reference_speed_right*60)/(2*PI))/0.229));
    }
    else
    {
      dxl1_wb.goalVelocity(dxl1_id, (int32_t)(((reference_speed_left*60)/(2*PI))/0.114));
      dxl2_wb.goalVelocity(dxl2_id, -(int32_t)(((reference_speed_right*60)/(2*PI))/0.114));
    }
  }
  else
  {
    if (digitalRead(pinRobot) == HIGH) 
    {
      dxl1_wb.goalVelocity(dxl1_id, (int32_t)(((reference_speed_left*60)/(2*PI))/0.229));
      dxl2_wb.goalVelocity(dxl2_id, (int32_t)(((reference_speed_right*60)/(2*PI))/0.229));
    }
    else
    {
      dxl1_wb.goalVelocity(dxl1_id, -(int32_t)(((reference_speed_left*60)/(2*PI))/0.114));
      dxl2_wb.goalVelocity(dxl2_id, (int32_t)(((reference_speed_right*60)/(2*PI))/0.114));
    }
  }
}


//  Fonction de contrôle Haut niveau: détermination des destinations et des actions du robot
void HighLevelControl() 
{ 
  imu();
  RobotSpeedEvaluation();
  Pose_Estimation();
  if (!frontPosOk)
    PositionControlAlgorithm();
}


//  Fonction permettant de déterminer les vitesses de consigne de chaque moteur
void PositionControlAlgorithm()
{
  //  Distances et angles nécessaires
  TD = sqrt((Pose_X_d-Pose_X_0)*(Pose_X_d-Pose_X_0)+(Pose_Y_d-Pose_Y_0)*(Pose_Y_d-Pose_Y_0));
  
  if (side)
  {
    Theta1 = -(sign(Pose_Y_d-Pose_Y_0)*acos((Pose_X_d-Pose_X_0)/TD));// atan((Pose_Y_d-Pose_Y_0)/(Pose_X_d-Pose_X_0));
  }
  if (!side) 
  {
    Theta1 = sign(Pose_Y_d-Pose_Y_0)*acos((Pose_X_d-Pose_X_0)/TD);// atan((Pose_Y_d-Pose_Y_0)/(Pose_X_d-Pose_X_0));
  }
  //Serial.println(Theta1);

  //  Erreurs
  double erreurAngle=SamplePeriod*(2*(Speed/8)*WheelRadius/WheelBase); //  erreur d'angle= vitesse de rotation du robot * periode d'échantillonnage  = angle parcouru entre deux échantillons
  double erreurPosition=((WheelRadius*Speed)*SamplePeriod)+TD*5*erreurAngle*fmax(sin(5*erreurAngle),cos(5*erreurAngle)); //  erreur de position majorée par (erreur de translation)*(1+erreurAngle)+TD*(erreurAngle)
  
  if ( ((Pose_X_d-erreurPosition)<=x && x<=(Pose_X_d+erreurPosition)) && ((Pose_Y_d-erreurPosition)<=y && y<=(Pose_Y_d+erreurPosition)) )
  {
    //  x=Pose_X_d, y=Pose_Y_d
    if ((Thetad-5*erreurAngle)<=Theta && Theta<=(Thetad+5*erreurAngle))
    {
      //  Theta=Thetad
      etat=1;
    }
    else
    {
      //  Theta!=Thetad
      etat=2;
    }
  }
  else
  { 
    //  x!=Pose_X_d, y!=Pose_Y_d
    if ( ((Theta1-5*erreurAngle)<=Theta && Theta<=(Theta1+5*erreurAngle)))
    {
      //  Theta=Theta1
      if ((Theta1-erreurAngle)<=Theta && Theta<=(Theta1+erreurAngle))
      {
        etat=3;
      }
      else 
      { 
        if(etat==3) 
        {
          etat=3;
        }
        else 
        {
          etat=4;
        }
      }
    }
    else 
    {
      //  Theta!=Theta1
      etat=4;
    }
  }
  
  //Détermination des vitesses de consigne de chaque moteur
  if (!frontPosOk) //  si le robot doit bouger
  {  
    int sens;
    switch (etat)
    {
      case 4:
        sens= rotationSign(Theta, Theta1);   //  détermination du sens de rotation du robot
        if((Theta1-Theta)>=0)
        {  
          if(reference_speed_left!=-Speed/8*sens && reference_speed_right!=Speed/8*sens)
          {
            //Serial.println("----- debut tournant - Etat4 -----"); 
            reference_speed_left=-Speed/8*sens;
            reference_speed_right=Speed/8*sens;
            LowLevelControl();
          }
        }
        else
        {
          if(reference_speed_left!=Speed/8*sens && reference_speed_right!=-Speed/8*sens)
          {
            //Serial.println("----- debut tournant - Etat4 -----"); 
            reference_speed_left=Speed/8*sens;    
            reference_speed_right=-Speed/8*sens;
            LowLevelControl();
          }
        }  
        break;
      case 3: 
        if(reference_speed_left!=Speed && reference_speed_right!=Speed)
        {
          //Serial.println("----- debut ligne droite - Etat3 -----");
          reference_speed_left=Speed;
          reference_speed_right=Speed;
          LowLevelControl();
        }
        break;
      case 2:
        sens=rotationSign(Theta,Thetad);
        if((Thetad-Theta)>=0)
        {
          if(reference_speed_left!=-Speed/8*sens && reference_speed_right!=Speed/8*sens)
          {
            //Serial.println("----- debut tournant - Etat2 -----"); 
            reference_speed_left=-Speed/8*sens;
            reference_speed_right=Speed/8*sens;
            LowLevelControl();
          }
        }
        else
        {
          if(reference_speed_left!=Speed/8*sens && reference_speed_right!=-Speed/8*sens)
          {
            //Serial.println("----- debut tournant - Etat2 -----"); 
            reference_speed_left=Speed/8*sens;    
            reference_speed_right=-Speed/8*sens;
            LowLevelControl();
          }
        }
        break;   
      case 1:
        //Serial.println("----- stop - Etat1 -----");
        reference_speed_left=0;
        reference_speed_right=0;
        LowLevelControl();
        frontPosOk=true;
        break;
      default:
        reference_speed_left=0;
        reference_speed_right=0;
        LowLevelControl();
        break;
    }
  } 
  else
  {
    reference_speed_left=0;
    reference_speed_right=0;
    LowLevelControl();
  }
}

//  Fonction permettant de donner une destination au robot (xd,yd,thetad) en arguments
void navigateTo(double targetx,double targety, double targetTheta) 
{
    if (side)
    {
      targetTheta = -targetTheta;
    }
    
    Pose_X_0=x;
    Pose_Y_0=y;
    Theta0=Theta;
    
    Pose_X_d=targetx;
    Pose_Y_d=targety;
    Thetad=targetTheta;

    Serial.print("Depart:");
    Serial.print(Pose_X_0);
    Serial.print(" ");
    Serial.print(Pose_Y_0);
    Serial.print(" ");
    Serial.println(Theta0);

    Serial.print("But:");
    Serial.print(Pose_X_d);
    Serial.print(" ");
    Serial.print(Pose_Y_d);
    Serial.print(" ");
    Serial.println(Thetad);
}


//  Fonction permettant de reculer
void rear() 
{
  if(sqrt(((Pose_X_0-x)*(Pose_X_0-x))+((Pose_Y_0-y)*(Pose_Y_0-y)))<rearDistance)
  {
     if(reference_speed_left!=-Speed && reference_speed_right!=-Speed)
     {
        Serial.println("----- debut marche arrière -----");
        reference_speed_left=-Speed;
        reference_speed_right=-Speed;
        if (digitalRead(pinRobot) == HIGH) 
        {
            dxl1_wb.goalVelocity(dxl1_id, (int32_t)(((reference_speed_left*60)/(2*PI))/0.229));
            dxl2_wb.goalVelocity(dxl2_id, (int32_t)(((reference_speed_right*60)/(2*PI))/0.229));
        }
        else
        {
            dxl1_wb.goalVelocity(dxl1_id, -(int32_t)(((reference_speed_left*60)/(2*PI))/0.114));
            dxl2_wb.goalVelocity(dxl2_id, (int32_t)(((reference_speed_right*60)/(2*PI))/0.114));
        }
     }
  }
  else
  {
    if(reference_speed_left!=0 && reference_speed_right!=0)
    {
      Serial.println("----- fin marche arrière -----");
      reference_speed_left=0;
      reference_speed_right=0;
      dxl1_wb.goalVelocity(dxl1_id, (int32_t)(((reference_speed_left*60)/(2*PI))/0.229));
      dxl2_wb.goalVelocity(dxl2_id, (int32_t)(((reference_speed_right*60)/(2*PI))/0.229));
      unloadGreen--;
      unloadRed--;
      backPosOk= true;
    }
  } 
}

//  Fonction qui compte le temps depuis le début du match
void tempsMatch()
{
  temps+=1;
}


//  Fonction permettant de fixer le phare comme destination
void goToLighthouse() 
{
  Serial.println("go to lighthouse");
  Stop=false;
  navigateTo(0.25,0.22,0);
}

//  Fonction permettant de fixer la girouette comme destination
void goToGirouette()
{  
  if (Stop) 
  {
    Stop=false;
    navigateTo(1,1,-PI/4);
  }
}


//  Fonction permettant de fixer la base nord pour mouiller
void goToNorth() 
{  
  if (Stop)
  {
    Stop=false;
    navigateTo(0.23,0.29,0);
  }
}


//  Fonction permettant de fixer la base sud pour mouiller
void goToSouth() 
{  
  if (Stop) 
  {
    Stop=false;
    navigateTo(0.23,1.2,0);
  } 
}


//Fonction permettant de hisser les pavillons
void hisserPavillons() 
{
  servoPavillon.write(90);
}

//  Fonction permettant de ranger les pavillons
void rangerPavillons()
{
  servoPavillon.write(10);
}


///////////////////////
/////Bouées
///////////////////////
//  Fonction permettant de déterminer les positions des balises rouges à ramasser comme destinations successives
void goTakeRedBuoys() 
{
  switch (compteurRouge) 
  {
    case 4:
      if(frontPosOk)
      {
        frontPosOk=false;
        navigateTo(1.065,1.650,PI/4); //  premiere bouée
        compteurRouge--;
      }
      break;
    case 3:
      if(frontPosOk)
      {
        frontPosOk=false;
        navigateTo(1.27,1.2,-PI/4);  // deuxieme bouée
        compteurRouge--;
      }
      break;
    case 2:
      if(frontPosOk)
      {
        frontPosOk=false;
        navigateTo(0.95,0.4,-PI/2);  // bouée 3
        compteurRouge--;
      }
      break;
    case 1:
      if(frontPosOk)
      {
        frontPosOk=false;
        Serial.println("dernière bouée");
        navigateTo(0.44,0.510,-PI); //quatrieme bouée
        compteurRouge--;
      }
      break;
    case 0:
      //on arrete les moteurs
      redLoaded=true;
      steps++;
      /*uint8_t dxl1_id = DXL_ID_1;
      uint8_t dxl2_id = DXL_ID_2;*/
      dxl1_wb.goalVelocity(dxl1_id, (int32_t)(0));
      dxl2_wb.goalVelocity(dxl2_id, (int32_t)(0));
      break;
  }
}

//  Fonction permettant de décharger les bouées rouges sur la ligne rouge du port
void unloadRedBuoys() 
{
  switch(unloadRed)
  {
    case 2:
      if(frontPosOk)
      {
        Serial.println("UNLOADING BUOYS");
        frontPosOk=false;
        navigateTo(0.20,0.51,-PI);
        unloadRed--;
      }
      break;
    case 1:
      if (frontPosOk)
      {
        if(backPosOk)
        {
          Serial.println("rear called");
          backPosOk = false;
          rearDistance=0.3;
          Pose_Estimation();
          Pose_X_0=x;
          Pose_Y_0=y;
        }
        rear();
      }
      break;
    case 0:
      redLoaded=false;
      steps++;
      Serial.println("bouées déchargées");
      break;
  }
}

//  Fonction permettant de déterminer les positions des bouées vertes à ramasser comme destinations successives
void goTakeGreenBuoys() 
{
  switch(compteurVert)
  {
    case 8:
      if(frontPosOk)
      {
        Serial.println("1ere bouée");
        frontPosOk= false;
        navigateTo(0.280,0.400,-PI/2); //  premiere bouée
        compteurVert--;
      }
      break;
    case 7:
      if(frontPosOk)
      {
        Serial.println("phare");
        frontPosOk= false;
        goToLighthouse();
        compteurVert--;
      }
      break;
    case 6:
      if(frontPosOk)
      {
        Serial.println("2eme bouée");
        frontPosOk= false;
        navigateTo(0.67,0.30,PI/4); //  'deuxième bouée'
        compteurVert--;
      }
      break;
    case 5:
      if(frontPosOk)
      {
        Serial.println("3eme bouée");
        frontPosOk=false;
        navigateTo(0.80,0.80,PI/4);  // positionnement bouée 3
        compteurVert--;
      }
      break;
    case 4:
      if(frontPosOk)
      {
        Serial.println("3&eme bouée");
        frontPosOk=false;
        navigateTo(1.10,0.80,0);  // bouée 3&
        compteurVert--;
      }
      break;
    case 3:
      if(frontPosOk)
      {
        Serial.println("4eme bouée");
        frontPosOk=false;
        navigateTo(1.73,1.20,PI/4);  // bouée 4
        compteurVert--;
      }
      break;
    case 2:
      if(frontPosOk)
      {
        Serial.println("5eme bouée");
        frontPosOk=false;
        navigateTo(1.27,1.45,3*PI/4);  // 'bouée 5'
        compteurVert--;
      }
      break;
    case 1:
      if(frontPosOk)
      {
        Serial.println("6eme bouée");
        frontPosOk=false;
        offset=0.15;  //erreur due au drift
        navigateTo(0.45,1.080,PI);  // bouée 6
        compteurVert--;
      }
      break;
    case 0:
      //on arrete les moteurs
      greenLoaded=true;
      steps++;
      /*uint8_t dxl1_id = DXL_ID_1;
      uint8_t dxl2_id = DXL_ID_2;*/
      dxl1_wb.goalVelocity(dxl1_id, (int32_t)(0));
      dxl2_wb.goalVelocity(dxl2_id, (int32_t)(0));
      break;
    }
}


//  Fonction permettant de décharger les bouées vertes sur la ligne verte du port
void unloadGreenBuoys() 
{  
  switch(unloadGreen)
  {
    case 2: 
      if(frontPosOk)
      {
        Serial.println("UNLOADING GREEN BUOYS");
        frontPosOk= false;
        navigateTo(0.20,1.08,PI);
        unloadGreen--;
      }
      break;
    case 1:
      if (frontPosOk)
      {
        if(backPosOk)
        {
          Serial.println("rear called");
          backPosOk = false;
          rearDistance=0.3;
          Pose_Estimation();
          Pose_X_0=x;
          Pose_Y_0=y;
        }
        rear();
      }
      break;
    case 0:
      greenLoaded=false;
      steps++;
      unloadRed++;    //compensation
      Serial.println("bouées déchargées");
      break;
  }
}

///////////////////////
///Setup
///////////////////////
void setup() 
{
  Serial.begin(57600);
  temps=0;  //  Initialisation du temps du match

  //  Détermination des modes de chaque pin utilisées (entrées)
  pinMode(pinRobot, INPUT);
  pinMode(pinNorth, INPUT);
  pinMode(pinSouth, INPUT);
  pinMode(pinSide, 1);
  pinMode(pinPushPavillon,1);
  pinMode(pinFinDeCourse, INPUT);
  pinMode(ledUser, OUTPUT);
  pinMode(ledUser2, OUTPUT);

  servoPavillon.attach(pinPavillons); //  Définition du pin relié à servoPavillon

  // Lit le bouton permettant de déterminer de quel côté le robot joue
  side = (digitalRead(pinSide)==1)?true:false;

  //  Initialisation des coordonnées du robot
  x=Pose_X_0;
  y=Pose_Y_0;
  
  //  Fonction d'initialisation des moteurs (pris de l'exemple)
  const char *log;
  bool result = false;
  uint16_t model_number = 0;

  result = (dxl1_wb.init(DEVICE_NAME, BAUDRATE, &log)&& dxl2_wb.init(DEVICE_NAME, BAUDRATE, &log));
  
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to init");
  }
  else 
  {
    Serial.print("Succeeded to init : ");
    Serial.println(BAUDRATE);  
  }

  result = (dxl1_wb.ping(dxl1_id, &model_number, &log) && dxl2_wb.ping(dxl2_id, &model_number, &log));
  
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to ping");
  }
  
  else 
  {
    Serial.println("Succeeded to ping");
    Serial.print("id : ");
    Serial.print(dxl1_id);
    Serial.print("   ");
    Serial.print("id : ");
    Serial.print(dxl2_id);
    Serial.print(" model_number : ");
    Serial.println(model_number);
  }

  result = (dxl1_wb.wheelMode(dxl1_id, 0, &log) && dxl2_wb.wheelMode(dxl2_id, 0, &log));
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to change wheel mode");
  }
  
  else 
  {
    Serial.println("Succeed to change wheel mode");
    Serial.println("Dynamixel is HighLevelControl...");
  }
  //  Fin de l'initialisation des moteurs

  //  Démarrage de l'IMU
  IMU.begin();

  //  Initialisation des Timer
  Timer.stop();
  Timer.setPeriod(1000);           // in microseconds
  Timer.attachInterrupt(tempsMatch);
  Serial.println("GO");
  Timer.start();
}

void loop() 
{
  //navigation
  switch (steps)
  {
    case 0:
      steps++;
      break;
    case 1:
      goTakeGreenBuoys();
      break;
    case 2:
      unloadGreenBuoys();
      break;
    case 3:
      goTakeRedBuoys();
      break;
    case 4:
      unloadRedBuoys();
      break;
  }
  
  //on actualise la position (pose estimation + going)
  if (temps-lastSampleTime>=SamplePeriod*1000) 
  {
    HighLevelControl();
    lastSampleTime=temps;
  }
}
