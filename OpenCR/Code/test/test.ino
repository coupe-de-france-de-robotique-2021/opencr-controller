/**
 * Programme final
 */

#include <DynamixelWorkbench.h>
#include <Servo.h>
#include <IMU.h>

void poseDrapeauFinal();
void poseDrapeauInitial();

#include "variables.h"
#include "communication.h"


void setup()
{
  pinMode(ledOnBoard, OUTPUT);
  digitalWrite(ledOnBoard, 1);
  Serial.begin(57600);

  initCommunication(2000000);
 
  while(!Serial); // Wait for Opening Serial Monitor
  Serial.println("Starting...");

  IMU.begin();
  IMU_Calibration();    //on demarre IMU et on calibre
  
//  Fonction d'initialisation des moteurs (pris de l'exemple)
  const char *log;  
  bool result = false;
  uint16_t model_number = 0;

  result = (dxl1_wb.init(DEVICE_NAME, BAUDRATE, &log)&& dxl2_wb.init(DEVICE_NAME, BAUDRATE, &log));
  
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to init");
  }
  else 
  {
    Serial.print("Succeeded to init : ");
    Serial.println(BAUDRATE);  
  }

  result = (dxl1_wb.ping(dxl1_id, &model_number, &log) && dxl2_wb.ping(dxl2_id, &model_number, &log));
  
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to ping");
  }
  
  else 
  {
    Serial.println("Succeeded to ping");
    Serial.print("id : ");
    Serial.print(dxl1_id);
    Serial.print("   ");
    Serial.print("id : ");
    Serial.print(dxl2_id);
    Serial.print(" model_number : ");
    Serial.println(model_number);
  }

  result = (dxl1_wb.wheelMode(dxl1_id, 0, &log) && dxl2_wb.wheelMode(dxl2_id, 0, &log));
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to change wheel mode");
  }
  
  else 
  {
    Serial.println("Succeed to change wheel mode");
    Serial.println("Dynamixel is HighLevelControl...");
  }
  //  Fin de l'initialisation des moteurs

  servoFlag.attach(pinServo);
  poseDrapeauInitial();

  //  Initialisation des Timer
  Timer.stop();
  Timer.setPeriod(1000);           // in microseconds
  Timer.attachInterrupt(tempsMatch);
  Serial.println("GO");
  Timer.start();

  dxl1_wb.goalVelocity(dxl1_id, 0);
  dxl2_wb.goalVelocity(dxl2_id, 0);

  startOk = true;                 //init termine
  digitalWrite(ledOnBoard, 0);    //temoin led
  /*xPosF= 0.3;
  yPosF= 0.3;
  thetaPosF=0;
  poseOk = false;
  backward = false;*/

  timeMove = millis();
  omegaConsigne = 5;
}

void loop()
{ 
  receptTrame();
  
  if (millis() - CurrentTime >= SampleTime * 1000)
  {
    //Serial.println(millis()-CurrentTime);
    IMU_Data();                   //données intertielle
    SpeedEvaluation() ;           //estimation de la vitesse
    poseEstimation();             //estimation de la position
    positionControlAlgorithm();   //strategie de navigation
    CurrentTime = millis();

    /*Serial.print(xPos);
    Serial.print("\t");
    Serial.print(yPos);
    Serial.print("\t");
    Serial.println(thetaPos);*/
    }
}

//  Fonction qui compte le temps depuis le début du match
void tempsMatch()
{
  temps+=1;
}

void SpeedEvaluation()
{
  uint8_t dxl1_id = DXL_ID_1;
  uint8_t dxl2_id = DXL_ID_2;
  const char *log;
  bool result = false;
  result = dxl1_wb.itemRead(dxl1_id, "Present_Velocity", &get_speed_right, &log)  && dxl2_wb.itemRead(dxl2_id, "Present_Velocity", &get_speed_left, &log);
  if (result == false)
  {
    Serial.println(log);
    Serial.println("Failed to read");
  }
  else
  {
    real_speed_right = get_speed_right * 0.229 / 60 * 2 * PI;
    real_speed_left = get_speed_left * 0.229 / 60 * 2 * PI;
    /*
      Serial.print(reference_left_speed);
      Serial.print(" \t");
      Serial.print(reference_right_speed);
      Serial.print(" \t");
      Serial.print(" \t");
      Serial.print(real_speed_left);
      Serial.print(" \t");
      Serial.println(real_speed_right);
    */
    //calcul de la vitesse du robot
    vRobot = RADIUS_WHEEL * (real_speed_left + real_speed_right) / 2;
    wRobot = RADIUS_WHEEL * (real_speed_left - real_speed_right) / EMPATTEMENT;
  }
}

void poseEstimation()
{
  //theta estimation
  //thetaPos = thetaPos+wRobot*SampleTime;      //estimation par integration => odometry
  thetaPos = tabIMU[3];            //estimation par IMU

  //on ajoute l'offset
  thetaPos+= offsetAngle;

  //cartesian estimation
  xPos = xPos + (vRobot * cos(thetaPos) * SampleTime) * COEF_CORRECTEUR;
  yPos = yPos + (vRobot * sin(thetaPos) * SampleTime) * COEF_CORRECTEUR;
}

void positionControlAlgorithm()
{
  //rotation
  if (millis()-timeMove < TIMEOUT)
  {
    timeMove = millis();
    if (omegaConsigne < 0)
    {
      reference_right_speed = -omegaConsigne;
      reference_left_speed = omegaConsigne;
    }
    else
    {
      reference_right_speed = omegaConsigne;
      reference_left_speed = -omegaConsigne;
    }

    dxl1_wb.goalVelocity(dxl1_id, (int32_t)(((reference_right_speed * 60) / (2 * PI)) / 0.229));
    dxl2_wb.goalVelocity(dxl2_id, (int32_t)(((reference_left_speed * 60) / (2 * PI)) / 0.229));
  }


  //avance
  //dxl1_wb.goalVelocity(dxl1_id, 0);
  //dxl2_wb.goalVelocity(dxl2_id, 0);
}

//recupere les données inertielles
void IMU_Data()
{ 
  static uint32_t tTime[3];
  static uint32_t imu_time = 0;

  tTime[2] = micros();
  if ( IMU.update() > 0 )
    imu_time = micros() - tTime[2];

  if ( (millis() - tTime[1]) >= 50 )   // on recupere les donnees toutes les 50ms
  {
    tTime[1] = millis();

    tabIMU[0] = imu_time ;
    tabIMU[1] = (IMU.rpy[0]) * 2 * 3.1415 / 360; //roll en radian
    tabIMU[2] = (IMU.rpy[1]) * 2 * 3.1415 / 360; //pitch en radian
    tabIMU[3] = (IMU.rpy[2]) * 2 * 3.1415 / 360; //yaw en radian
  }

}

//fonction de calibrage de l'imu
void IMU_Calibration()
{
  Serial.println("ACC Cali Start\n");

  IMU.SEN.acc_cali_start();
  while ( IMU.SEN.acc_cali_get_done() == false )
  {
    IMU.update();
  }

  Serial.print("ACC Cali End\n");
}

void poseDrapeauInitial()
{
  servoFlag.write(165);
}

void poseDrapeauFinal()
{
  servoFlag.write(90); 
}
