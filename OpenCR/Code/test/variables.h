//  Détermination du type de carte utilisé (ici OPENCR) automatiquement
#if defined(__OPENCM904__)
  #define DEVICE_NAME "3" //Dynamixel on Serial3(USART3)  <-OpenCM 485EXP
#elif defined(__OPENCR__)
  #define DEVICE_NAME ""
#endif   


#define EMPATTEMENT 0.20      //a mesurer precisement 
#define RADIUS_WHEEL 0.0325

#define COEF_CORRECTEUR 45

#define pinServo 57
#define ledOnBoard 22

#define tempsmatch 10000000
double temps;               //  temps passé depuis le début du match (ms)

float Speed = 6.35; // rad/s  // Reference speed of the displacement  MAX +/- 6.35 rad/s
float SpeedRotation = 4;    //on divise la vitesse lors des rotations

//  Initilisation des moteurs
#define BAUDRATE  57600     
#define DXL_ID_1   1        // ID of RIGHT DYNAMIXEL 
#define DXL_ID_2   2        // ID of LEFT DYNAMIXEL
DynamixelWorkbench dxl1_wb, dxl2_wb;  //  création de l'instance de chaque moteur
uint8_t dxl1_id = DXL_ID_1;
uint8_t dxl2_id = DXL_ID_2;

//  Création d'un timer (HardwareTimer est inclu dans la bibliothèque DynamixelWorkbench)
HardwareTimer Timer(TIMER_CH1);


float reference_left_speed = Speed, reference_right_speed = Speed, real_left_speed, real_right_speed;
float real_speed_right = 0;
float real_speed_left = 0;
int32_t get_speed_right = 0;
int32_t get_speed_left = 0;
double CurrentTime; //ms
float SampleTime = 0.001;    //en sec
float timeUpdateIUM = 50;   //en ms

bool poseOk = true;
bool backward = false;
bool startOk  = false;


cIMU    IMU;
double tabIMU[4];     //  tableau permettant de récupérer les différentes données données par l'IMU

Servo servoFlag;      //  servo permettant de bouger le drapeau

//robot
double xPos = 0;
double yPos = 0;
double thetaPos = 0;
double vRobot = 0;    //vitesse lineaire
double wRobot = 0;    //vitesse angulaire

//navigation
int etat =0;      //etat de navigation
double TD;
double teta1;
double offsetAngle=0;

//consigne
double omegaConsigne=0;
double xLineaireConsigne=0;
double yLineaireConsigne =0;

int TIMEOUT = 3000;
int timeMove = 0;
