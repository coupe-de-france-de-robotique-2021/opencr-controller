//macro de l'uart
byte UART_PRET = 0x01;
byte UART_CALIBRATE = 0x02;
byte UART_MOVE = 0x03;
byte UART_STOP = 0x04;
byte UART_POSE = 0x05;
byte UART_FLAG = 0x06;

//points
double ptF_x=0, ptF_y=0;
double thetaRecep;

//initialisation
void initCommunication(int bauds)
{
  Serial1.begin(bauds);
}

//Determine le signe de nb1-nb2
int sign(double nb1, double nb2)
{
  return (nb1 > nb2)? 1:-1;
}

//fonction permettant de convertir un double en byte array
byte* doubleToByteArray(double nb)
{
  static byte tab[8];
  *((double *)(tab)) = nb;
  return tab;
}

//fonction permettant de convertir un byte array en double
double byteArrayToDouble(byte tab[8])
{
  return *((double *)(tab));
}


///////////////////////////
//////Ordre sended
///////////////////////////

//robot pret
void returnRobotReady()
{
  Serial1.write(UART_PRET);
}

//ordre de retour si on a atteint la position voulue
void returnAvanceOk()
{
  byte *tab;    //buffer
  
  //trame
  Serial1.write(UART_MOVE);

  //direction
  byte dir;
  if (!backward)
    dir = 0x01;
  else
    dir = 0x00;
  
  Serial1.write(dir);

  //pose
  tab = doubleToByteArray(ptF_x);
  Serial1.write(tab, 8);
  tab = doubleToByteArray(ptF_y);
  Serial1.write(tab, 8);
  tab = doubleToByteArray(thetaRecep);
  Serial1.write(tab, 8);
  Serial.println("Envoie");
}

//retourne la pose du robot
void returnPose()
{
   byte *tab;    //buffer

  //trame
  Serial1.write(UART_POSE);

  double angle = thetaPos;
  if (angle >= PI)
    angle -= PI;
  if (angle <= -PI)
    angle += PI;
  
  tab = doubleToByteArray(xPos);
  Serial1.write(tab, 8);
  tab = doubleToByteArray(yPos);
  Serial1.write(tab, 8);
  tab = doubleToByteArray(angle);
  Serial1.write(tab, 8);
}
////////////////////////////////////////////////////////////



/////////////////////////////////////
/////Ordre received
/////////////////////////////////////

//ordre est ce que le robot est pret
void ordrePret()
{
  if (startOk)
    returnRobotReady();
}

//ordre de calibration
void ordreCali()
{
  byte tab[8];             //buffer
  
  Serial1.readBytes(tab, 8);
  ptF_x = byteArrayToDouble(tab);

  Serial1.readBytes(tab, 8);
  ptF_y = byteArrayToDouble(tab);

  Serial1.readBytes(tab, 8);
  thetaRecep = byteArrayToDouble(tab);

  xPos = ptF_x;
  yPos = ptF_y;
  offsetAngle = thetaRecep;

//  if (poseOk)
//  {
//    TD = sqrt(pow((xPosF-xPos),2)+pow((yPosF-yPos),2));
//    teta1 = sign(yPosF,yPos)*acos((xPosF-xPos)/TD);
//    if (teta1 > (PI/2) || teta1 < (-PI/2))
//      backward = true;
//  }
//  poseOk = false;
}

//commande pour faire avancer le robot
void ordreAvance()
{  
  poseOk = false;
  
  byte tab[8];                                    //buffer

  //avance
  byte recep[1];
  Serial1.readBytes(recep, 1);
  int dir = int(recep[0]);
  backward = (dir != 1);

  //pose voulue
  Serial1.readBytes(tab, 8);
  ptF_x = byteArrayToDouble(tab);

  Serial1.readBytes(tab, 8);
  ptF_y = byteArrayToDouble(tab);

  Serial1.readBytes(tab, 8);
  thetaRecep = byteArrayToDouble(tab);


  //Position finale désirée
  //xPosF = ptF_x;
  //yPosF = ptF_y;
  //thetaPosF = thetaRecep;

  /*Serial.println(backward);
  Serial.println(xPosF);
  Serial.println(yPosF);
  Serial.println(thetaPosF);*/
}

//commande pour stopper le robot
void ordreStop()
{
  //on stoppe les moteurs
  poseOk = true;
  dxl1_wb.goalVelocity(dxl1_id, 0);
  dxl2_wb.goalVelocity(dxl2_id, 0);
}

//commande pour envoyer la pose du robot
void ordrePose()
{  
  returnPose();
}

void ordreFlag()
{
  poseDrapeauFinal();
}

////////////////////////////////////////////////////////////

void receptTrame()
{  
  if(Serial1.available())
  {
    byte buf = Serial1.read();
    
    //differents ordre
    if (buf == UART_PRET)
    {
      Serial.println("pret");
      ordrePret();
    }
    else if (buf == UART_CALIBRATE)
    {
      Serial.println("cali");
      ordreCali();
    }
    else if (buf == UART_MOVE)
    {
      Serial.println("move");
      ordreAvance();
    }
    else if (buf == UART_STOP)
    {
      Serial.println("stop");
      ordreStop();
    }
    else if (buf == UART_POSE)
    {
      Serial.println("pose");
      ordrePose();
    }
    else if (buf == UART_FLAG)
    {
      Serial.println("drapeau");
      ordreFlag();
    }
  }
}
