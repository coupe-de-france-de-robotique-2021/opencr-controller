/**
 * 
 */


#include <DynamixelWorkbench.h>
#include <IMU.h>

//  Détermination du type de carte utilisé (ici OPENCR) automatiquement
#if defined(__OPENCM904__)
  #define DEVICE_NAME "3" //Dynamixel on Serial3(USART3)  <-OpenCM 485EXP
#elif defined(__OPENCR__)
  #define DEVICE_NAME ""
#endif   


#define EMPATTEMENT 0.20      //a mesurer precisement 
#define RADIUS_WHEEL 0.0325

#define COEF_CORRECTEUR 1.78

#define tempsmatch 10000000
double temps;               //  temps passé depuis le début du match (ms)

float Speed = 5; // rad/s  // Reference speed of the displacement  MAX +/- 6.35 rad/s
float SpeedRotation = 0.9;    //on divise la vitesse lors des rotations

//  Initilisation des moteurs
#define BAUDRATE  57600     
#define DXL_ID_1   1        // ID of RIGHT DYNAMIXEL 
#define DXL_ID_2   2        // ID of LEFT DYNAMIXEL
DynamixelWorkbench dxl1_wb, dxl2_wb;  //  création de l'instance de chaque moteur
uint8_t dxl1_id = DXL_ID_1;
uint8_t dxl2_id = DXL_ID_2;

//  Création d'un timer (HardwareTimer est inclu dans la bibliothèque DynamixelWorkbench)
HardwareTimer Timer(TIMER_CH1);


float reference_left_speed = Speed, reference_right_speed = Speed, real_left_speed, real_right_speed;
float real_speed_right = 0;
float real_speed_left = 0;
int32_t get_speed_right = 0;
int32_t get_speed_left = 0;
double CurrentTime; //ms
float SampleTime = 0.05;

cIMU    IMU;
double tabIMU[4]; //  tableau permettant de récupérer les différentes données données par l'IMU

//robot
double xPos = 0;
double yPos = 0;
double thetaPos = 0;
double vRobot = 0;    //vitesse lineaire
double wRobot = 0;    //vitesse angulaire

//navigation
int etat =0;      //etat de navigation
double TD;
double teta1;
//position final désirée
double xPosF = 0;
double yPosF = 0;
double thetaPosF = 0;
//position initiale
double xPos0=0;
double yPos0=0;
double thetaPos0=0;


void setup()
{
  Serial.begin(57600);
  IMU.begin();
  IMU_Calibration();    //on demarre IMU et on calibre


  //while(!Serial); // Wait for Opening Serial Monitor

//  Fonction d'initialisation des moteurs (pris de l'exemple)
  const char *log;
  bool result = false;
  uint16_t model_number = 0;

  result = (dxl1_wb.init(DEVICE_NAME, BAUDRATE, &log)&& dxl2_wb.init(DEVICE_NAME, BAUDRATE, &log));
  
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to init");
  }
  else 
  {
    Serial.print("Succeeded to init : ");
    Serial.println(BAUDRATE);  
  }

  result = (dxl1_wb.ping(dxl1_id, &model_number, &log) && dxl2_wb.ping(dxl2_id, &model_number, &log));
  
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to ping");
  }
  
  else 
  {
    Serial.println("Succeeded to ping");
    Serial.print("id : ");
    Serial.print(dxl1_id);
    Serial.print("   ");
    Serial.print("id : ");
    Serial.print(dxl2_id);
    Serial.print(" model_number : ");
    Serial.println(model_number);
  }

  result = (dxl1_wb.wheelMode(dxl1_id, 0, &log) && dxl2_wb.wheelMode(dxl2_id, 0, &log));
  if (result == false) 
  {
    Serial.println(log);
    Serial.println("Failed to change wheel mode");
  }
  
  else 
  {
    Serial.println("Succeed to change wheel mode");
    Serial.println("Dynamixel is HighLevelControl...");
  }
  //  Fin de l'initialisation des moteurs


  //  Initialisation des Timer
  Timer.stop();
  Timer.setPeriod(1000);           // in microseconds
  Timer.attachInterrupt(tempsMatch);
  Serial.println("GO");
  Timer.start();
  

  //Position finale désirée
  xPosF = 0.1;
  yPosF = 0;
  thetaPosF = 0;
  /* Strategie de navigation on va en ligne de la position 0 (xPos0, yPos0, thetaPos0) à la postion F (xPosF, yPosF, thetaPosF)
   * Il faudrait utiliser les données du lidar dans la navigation pour detecter un obstacle et determiner un point intermediaire pour eviter l'obstacle
   */
}

void loop()
{
  if (millis() - CurrentTime >= SampleTime * 1000)
  {
    //Serial.println(millis()-CurrentTime);
    SpeedEvaluation() ;           //estimation de la vitesse
    IMU_Data();                   //données intertielle
    poseEstimation();             //estimation de la position
    positionControlAlgorithm();   //strategie de navigation
    CurrentTime = millis();

    Serial.print(xPos);
    Serial.print("\t");
    Serial.print(yPos);
    Serial.print("\t");
    Serial.println(thetaPos);
  }
}

//  Fonction qui compte le temps depuis le début du match
void tempsMatch()
{
  temps+=1;
}

void SpeedEvaluation()
{
  uint8_t dxl1_id = DXL_ID_1;
  uint8_t dxl2_id = DXL_ID_2;
  const char *log;
  bool result = false;
  result = dxl1_wb.itemRead(dxl1_id, "Present_Velocity", &get_speed_right, &log)  && dxl2_wb.itemRead(dxl2_id, "Present_Velocity", &get_speed_left, &log);
  if (result == false)
  {
    Serial.println(log);
    Serial.println("Failed to read");
  }
  else
  {
    real_speed_right = get_speed_right * 0.229 / 60 * 2 * PI;
    real_speed_left = get_speed_left * 0.229 / 60 * 2 * PI;
    /*
      Serial.print(reference_left_speed);
      Serial.print(" \t");
      Serial.print(reference_right_speed);
      Serial.print(" \t");
      Serial.print(" \t");
      Serial.print(real_speed_left);
      Serial.print(" \t");
      Serial.println(real_speed_right);
    */
    //calcul de la vitesse du robot
    vRobot = RADIUS_WHEEL * (real_speed_left + real_speed_right) / 2;
    wRobot = RADIUS_WHEEL * (real_speed_left - real_speed_right) / EMPATTEMENT;

  }
}

void poseEstimation()
{
  //theta estimation
  //thetaPos = thetaPos+wRobot*SampleTime;      //estimation par integration => odometry
  thetaPos = tabIMU[3];            //estimation par IMU

  //on ajoute la position intiale
  thetaPos+= thetaPos0;

  //cartesian estimation
  xPos = xPos + (vRobot * cos(thetaPos) * SampleTime) * COEF_CORRECTEUR;
  yPos = yPos + (vRobot * sin(thetaPos) * SampleTime) * COEF_CORRECTEUR;
}


void positionControlAlgorithm()
{
  TD = sqrt(pow((xPosF-xPos0),2)+pow((yPosF-yPos0),2));
  teta1 = sign(yPosF,yPos0)*acos((xPosF-xPos0)/TD)-thetaPos0;
  Serial.println(teta1);

  //distance restante
  double distRestante = sqrt(pow((xPosF-xPos),2)+pow((yPosF-yPos),2));

  //erreur autorisée
  double erreurAngle = 0.03;       //en radian
  double erreurDistance = 0.03;    //en m
  
  //on calcule l'etat
  if(thetaPos > (thetaPosF-erreurAngle) && thetaPos < (thetaPosF+erreurAngle) && (distRestante < erreurDistance))
    etat = 4;                                                                                                        //position angulaire finale atteinte et position cartesienne finale atteinte
  else if (distRestante < erreurDistance)
    etat = 3;                                                                                                        //position cartesienne atteinte => mise en orientation finale
  else if (thetaPos > (teta1-erreurAngle) && thetaPos < (teta1+erreurAngle))
    etat = 2;                                                                                                        //position angulaire pour parcourir la ligne droite atteinte => on avance tout droit
  else
    etat = 1;                                                                                                        //on s'oriente pour parcourir la ligne droite
  
  switch(etat)
  {
    case 1:                                       //orientation du robot vers la direction souhaité
      if((teta1+thetaPos0) < thetaPos)
      {
        reference_right_speed = SpeedRotation;
        reference_left_speed = -SpeedRotation;
      }
      if((teta1+thetaPos0) > thetaPos)
      {
        reference_right_speed = -SpeedRotation;
        reference_left_speed = SpeedRotation;
      }
      break;
    case 2:                                       //on avance tout droit
      reference_right_speed = Speed;
      reference_left_speed = Speed;
      break;
    case 3:                                       //on s'oriente selon l'angle final
      if (thetaPosF < thetaPos)
      {
        reference_right_speed = SpeedRotation;
        reference_left_speed = -SpeedRotation;
      }
      if (thetaPosF > thetaPos)
      {
        reference_right_speed = -SpeedRotation;
        reference_left_speed = SpeedRotation;
      }
      break;
  case 4:                                         //on bouge plus
      reference_right_speed = 0;
      reference_left_speed = 0;
      break;
  }
  dxl1_wb.goalVelocity(dxl1_id, (int32_t)(((reference_right_speed * 60) / (2 * PI)) / 0.229));
  dxl2_wb.goalVelocity(dxl2_id, (int32_t)(((reference_left_speed * 60) / (2 * PI)) / 0.229));

}

//Determine le signe de nb1-nb2
int sign(double nb1, double nb2)
{
  return (nb1 > nb2)? 1:-1;
}

//recupere les données inertielles
void IMU_Data()
{
  static uint32_t tTime[3];
  static uint32_t imu_time = 0;

  tTime[2] = micros();
  if ( IMU.update() > 0 )
    imu_time = micros() - tTime[2];

  if ( (millis() - tTime[1]) >= 50 )   // on recupere les donnees toutes les 150ms
  {
    tTime[1] = millis();

    tabIMU[0] = imu_time ;
    tabIMU[1] = (IMU.rpy[0]) * 2 * 3.1415 / 360; //roll en radian
    tabIMU[2] = (IMU.rpy[1]) * 2 * 3.1415 / 360; //pitch en radian
    tabIMU[3] = (IMU.rpy[2]) * 2 * 3.1415 / 360; //yaw en radian
    Serial.println(tabIMU[3]);
  }

}

//fonction de calibrage de l'imu
void IMU_Calibration()
{
  Serial.println("ACC Cali Start\n");

  IMU.SEN.acc_cali_start();
  while ( IMU.SEN.acc_cali_get_done() == false )
  {
    IMU.update();
  }

  Serial.print("ACC Cali End\n");
}
