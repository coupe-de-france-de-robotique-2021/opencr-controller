#include <DynamixelWorkbench.h>     //  Bibliothèque liée au moteurs
#define COM 51      //fil vert
//NO (noir) ou NC (rouge) relié à la masse
//si NO connecte signal COM à 1 si gachette relachee

void setup()
{
  Serial.begin(9600);
  pinMode(COM, INPUT_PULLUP);
}

void loop() 
{
  Serial.print(digitalRead(COM));
  delay(1000);
}
